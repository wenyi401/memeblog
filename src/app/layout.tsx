import './globals.css'
import type { Metadata } from 'next'
import { ThemeProvider } from "@/components/theme-provider"
import { ModeToggle } from "@/components/mode-toggle"
import Link from "next/link"


export const metadata: Metadata = {
  title: 'Mysite',
  description: 'a blog made by love',
}
 
export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body
        className="antialiased min-h-screen bg-white dark:bg-slate-950 text-slate-900 dark:text-slate-50"
      >
        <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
          <div className="maw-w-2xl mx-auto py-10 px-4">
        <header>
          <div className="flex items-center">
            <nav className="font-medium space-x-6">
            <Link href="/">Posts</Link>
            </nav>
          </div>
        </header>
        {children}
        <ModeToggle />
        </div>
        </ThemeProvider>
        </body>
    </html>
  )
}
