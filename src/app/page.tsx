import { allPosts } from "@contentlayergenerated"
import Link from "next/link"

export default function Home() {
  return (
    <main className="prose dark:prose-invert">
      {allPosts.map((post) => (
        <article key={post._id}>
          <Link href={post.slug}>
            <h2>{post.title}</h2>
          </Link>
          {post.description && <p>{post.description}</p>}
        </article>
      ))}
    </main>
  )
}
