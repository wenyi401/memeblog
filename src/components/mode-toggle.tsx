'use client'

import { useTheme } from 'next-themes'

export function ModeToggle() {
    const { setTheme, theme } =useTheme()

    return (
        <button
        onClick={() => setTheme(theme === "light" ? "dark" : "light")}>
            Toggle theme
        </button>
    )
}